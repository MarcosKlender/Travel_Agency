[![License](https://img.shields.io/apm/l/vim-mode.svg)](https://opensource.org/licenses/MIT) ![Development](https://img.shields.io/badge/development-done-green.svg)
# Travel Agency
Animación sobre una agencia de viajes ficticia con diferentes servicios a ofrecer.

## Requerimientos
- [Adobe Animate CC](https://www.adobe.com/la/products/animate.html)

## Instalación
Clonar o descargar el `.zip` del proyecto.

## Uso
El presente proyecto está dividido en 3 carpetas diferentes.

- **Interfaz:** Aquí se encuentra gran parte del proyecto, desde los archivos `.swf` hasta el contenido multimedia utilizado.

- **Intro:** Aquí se encuentra una plantilla utilizada para agregar una introducción al proyecto.

- **Juego:** Aquí se encuentra el juego animado que se utiliza en el proyecto.

Haga uso del software requerido para ver, editar y generar el presente proyecto.

## Solución de Problemas
Edite las direcciones URL del proyecto con la ubicación de los directorios que usted tenga en su ordenador.
